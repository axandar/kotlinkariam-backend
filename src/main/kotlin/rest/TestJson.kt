package rest

import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.*
import io.ktor.server.engine.applicationEngineEnvironment
import io.ktor.server.engine.connector
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.time.LocalDate

fun main(args: Array<String>) {
    val env = applicationEngineEnvironment {
        module {
            install(ContentNegotiation) {
                jackson {
                    configure(SerializationFeature.INDENT_OUTPUT, true)
                    setDefaultPrettyPrinter(DefaultPrettyPrinter().apply {
                        indentArraysWith(DefaultPrettyPrinter.FixedSpaceIndenter.instance)
                        indentObjectsWith(DefaultIndenter("  ", "\n"))
                    })
                }
            }
            jsonServing()
        }

        connector {
            host = "127.0.0.1"
            port = 9090
        }
    }

    embeddedServer(Netty, env).start(wait = true)
}

fun Application.jsonServing(){
    routing {
        route("/testJson"){
            route("/buildings"){
                building()
            }
            get{
                call.respondText("Works")
            }
        }
    }
}

fun Route.building(){
    route("/wall"){
        get{
            call.respond(model)
        }
    }
    route("/townCenter"){
        get{
            call.respond(model)
        }
    }
    route("/academia"){
        get{
            call.respond(model)
        }
    }
    route("/building"){
        get{
            call.respond(model)
        }
    }
}


data class Model(val name: String, val items: List<Item>, val date: LocalDate = LocalDate.of(2018, 4, 13))
data class Item(val key: String, val value: String)
val model = Model("root", listOf(Item("A", "Apache"), Item("B", "Bing")))


